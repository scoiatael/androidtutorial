package com.example.etern.androidtutorial;

import java.net.URI;
import java.util.List;

/**
 * Created by etern on 19/06/2016.
 */
public class MovieListWrapper {
    public Integer total;
    public List<Movie> movies;
}


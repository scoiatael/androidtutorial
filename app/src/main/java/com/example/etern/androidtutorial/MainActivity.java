package com.example.etern.androidtutorial;

import android.app.DownloadManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.etern.androidtutorial.MESSAGE";
    public static final String URL = "https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Glide.with(this)
                .load(R.drawable.loading)
                .asGif()
                .into(imageView);

        downloadData();
    }

    public void downloadData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        viewData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", error.toString());
            }
        });
        queue.add(stringRequest);
    }

    public void viewData(String message) {
        Intent intent = new Intent(this, MovieListActivity.class);

        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}

package com.example.etern.androidtutorial;

import java.net.URI;

/**
 * Created by etern on 19/06/2016.
 */
public class Posters {
    public String thumbnail;
    public String profile;
    public String detailed;
    public String original;
}

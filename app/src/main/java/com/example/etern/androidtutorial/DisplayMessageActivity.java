package com.example.etern.androidtutorial;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;


/**
 * Created by etern on 18/06/2016.
 */
public class DisplayMessageActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MovieListActivity.MOVIE_MESSAGE);

        Gson gson = new Gson();
        Movie movie = gson.fromJson(message, Movie.class);
        TextView textView = (TextView) findViewById(R.id.content);
        textView.setText(movie.title);

        ImageView imageView = (ImageView) findViewById(R.id.imageView2);
        Glide.with(this)
                .load(movie.posters.detailed)
                .into(imageView);
    }
}

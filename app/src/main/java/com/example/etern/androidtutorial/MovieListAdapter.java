package com.example.etern.androidtutorial;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by etern on 19/06/2016.
 */
public class MovieListAdapter extends ArrayAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private final static Integer layout = R.layout.simple_list_item_1;

    private List<Movie> movieList;

    public MovieListAdapter(Context context, List<Movie> movieList) {
        super(context, layout, movieList);

        this.context = context;
        this.movieList = movieList;

        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = layoutInflater.inflate(layout, parent, false);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView3);

        Glide
                .with(context)
                .load(movieList.get(position).posters.detailed)
                .override(200, 200)
                .into(imageView);

        TextView textView = (TextView) convertView.findViewById(R.id.textView);
        textView.setText(movieList.get(position).title);

        return convertView;
    }
}

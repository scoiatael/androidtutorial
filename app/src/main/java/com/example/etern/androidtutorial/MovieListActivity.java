package com.example.etern.androidtutorial;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

public class MovieListActivity extends ListActivity {

    public static final String MOVIE_MESSAGE = "com.example.etern.androidtutorial.MOVIE" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        Gson gson = new Gson();
        final MovieListWrapper movieListWrapper = gson.fromJson(message, MovieListWrapper.class);

        MovieListAdapter movieListAdapter = new MovieListAdapter(this, movieListWrapper.movies);

        ListView listView = getListView();
        listView.setAdapter(movieListAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showMovie(movieListWrapper.movies.get(i));
            }
        });
    }

    private void showMovie(Movie movie) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);

        Gson gson = new Gson();
        intent.putExtra(MOVIE_MESSAGE, gson.toJson(movie));
        startActivity(intent);
    }
}

package com.example.etern.androidtutorial;

public class Movie {
    public String id;
    public String title;
    public Posters posters;

    @Override
    public String toString() {
        return title;
    }
}
